import image1 from '../img/sedan-523x320-crop.jpg'
import bgimage1 from '../img/bg_1.png'







class block {
  constructor(options) {
    this.options = options;
  }
}

export class Template450 extends block {
  constructor(options) {
    super(options)
  }

  toHtml() {
    const { tagClass = 'abn-1', title, text1, text2, bgimage = bgimage1 ,imgW = 450 , imgH = 450, width = 450, height = 450} = this.options
    return `
    <div class="abn ${tagClass}" style="width:${width}px;height:${height}px;">
    <div class="abn-caption"
      style="background-image:url(${bgimage});">
      <div class="abn-caption__text abn-caption__text-center">
        <div class="abn-caption__text-large" contenteditable="true">${title}</div>
        <div class="abn-caption__text-origin" contenteditable="true">${text1}</div>
      </div>
    </div>
    <div class="abn-figure">
      <div class="img-preview preview-lg" style="width:${imgW}px;height:${imgH}px;"></div>
    </div>
  </div>`
  }
}

export class crop extends block{
  constructor(options){
    super(options)
  }

  toHtml(){
    const {image} = this.options
    return `<div class="col-sm">
    <img id="image" src="${image}" alt="Picture">
    
    <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
          <input type="file" class="sr-only" id="inputImage" name="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
          <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
            <span class="fa fa-upload"></span>
          </span>
        </label>

  </div>`
  }
}


