import firebase from 'firebase/app';


const firebaseConfig = {
  apiKey: "AIzaSyCyjlLSagy3mH0XHm2NY3ELrU5CtrEDmcA",
  authDomain: "imagegen-b5235.firebaseapp.com",
  databaseURL: "https://imagegen-b5235.firebaseio.com",
  projectId: "imagegen-b5235",
  storageBucket: "imagegen-b5235.appspot.com",
  messagingSenderId: "773265536372",
  appId: "1:773265536372:web:55e624abac2ccaee922821"
};

var firebaseApp = firebase.initializeApp(firebaseConfig);


var firebaseui = require('firebaseui');

var ui = new firebaseui.auth.AuthUI(firebase.auth());

var uiConfig = {
  callbacks: {
    signInSuccessWithAuthResult: function(authResult, redirectUrl) {
      // User successfully signed in.
      // Return type determines whether we continue the redirect automatically
      // or whether we leave that to developer to handle.
      return true;
    },
    uiShown: function() {
      // The widget is rendered.
      // Hide the loader.
      document.getElementById('loader').style.display = 'none';
    }
  },
  // Will use popup for IDP Providers sign-in flow instead of the default, redirect.
  signInFlow: 'popup',
  signInSuccessUrl: '<url-to-redirect-to-on-success>',
  signInOptions: [
    // Leave the lines as is for the providers you want to offer your users.
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
  ],
  // Terms of service url.
  tosUrl: '<your-tos-url>',
  // Privacy policy url.
  privacyPolicyUrl: '<your-privacy-policy-url>'
};

ui.start('#firebaseui-auth-container', {
  signInOptions: [
    // List of OAuth providers supported.
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
  ],
  // Other config options...
});
ui.start('#firebaseui-auth-container', uiConfig);