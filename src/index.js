import 'bootstrap'
import './styles/main.scss'


import 'canvas-to-image';
import imgageUpdate from './crop'
import './auth'

import html2canvas from 'html2canvas';







import { model } from "./model"


document.addEventListener('DOMContentLoaded', jopa, true);
function jopa() {
  //подставляем в страницу шаблон
  document.querySelector(".docs-preview--container").innerHTML = model[0].toHtml()
}





document.getElementById('download').addEventListener('click', toCanvas, true);

// function download() {
//     document.querySelectorAll("canvas").forEach((element) => {
//         canvasToImage(element, {
//             name: 'myImage',
//             type: 'jpg',
//             quality: 1
//         })
//     })
// }

function toCanvas() {
  html2canvas(document.querySelector(".abn"), { allowTaint: true }).then(function (canvas) {
    document.querySelector(".viewbox > div").appendChild(canvas);
  });
}


function draw() {
  var ctx = document.getElementById('canvas').getContext('2d');
  var img = new Image();
  img.onload = function () {
    for (var i = 0; i < 4; i++) {
      for (var j = 0; j < 3; j++) {
        ctx.drawImage(img, j * 50, i * 38, 50, 38);
      }
    }
  };
  img.src = 'https://mdn.mozillademos.org/files/5397/rhino.jpg';
}

//Поиск картинок
// function hndlr(response) {

//   JSON.parse(response).items.map(e => e.link).forEach(function (e, i) {

//     let canvas = document.createElement("canvas")
//     canvas.id = `canvas ${i}`
//     canvas.addEventListener('click', function () { imgageUpdate(this) })

//     canvas = document.body.appendChild(canvas)

//     var ctx = document.getElementById(`canvas ${i}`).getContext('2d')

//     var img = new Image();
//     img.onload = function () {
//       ctx.drawImage(img, 0, 0); // Or at whatever offset you like
//     };
//     img.crossOrigin = "anonymous";
//     img.src = e;
    

//     // document.querySelector(".test")
//     // .appendChild(img1)
//     // //добавляем заливку в кропер по клику
//     // .addEventListener('click',function () {
//     //   imgageUpdate(this)
//     // })
//   })
// }
// var theUrl = "https://www.googleapis.com/customsearch/v1?key=AIzaSyCN5d_eV6mAXjKJx_CmS1wOx6-bmTDYHxY&cx=1d6f925fa436f0b11&q=%D0%BC%D0%B5%D1%80%D1%81%D0%B5%D0%B4%D0%B5%D1%81&searchType=image"
// function httpGetAsync(url, callback) {
//   var xmlHttp = new XMLHttpRequest();
//   xmlHttp.onreadystatechange = function () {
//     if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
//       callback(xmlHttp.responseText);
//   }
//   xmlHttp.open("GET", theUrl, true); // true for asynchronous 
//   xmlHttp.send(null);
// }
// httpGetAsync(theUrl, hndlr)



//Размер шрифта
document.querySelector("#font-up").addEventListener('click', function () {
  let selection = window.getSelection();
  if (selection) {
    if (selection.baseNode.parentNode.classList.value.includes('abn')) {
      let size = window.getComputedStyle(selection.anchorNode.parentElement, null).getPropertyValue('font-size');
      selection.focusNode.parentElement.style.setProperty('font-size', `${parseInt(size) + 1}px`)
    }
  }
});

document.querySelector("#font-down").addEventListener('click', function () {
  let selection = window.getSelection();
  if (selection) {
    if (selection.baseNode.parentNode.classList.value.includes('abn')) {
      let size = window.getComputedStyle(selection.anchorNode.parentElement, null).getPropertyValue('font-size');
      selection.focusNode.parentElement.style.setProperty('font-size', `${parseInt(size) - 1}px`)
    }
  }
});