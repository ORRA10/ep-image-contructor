class Sources {
  constructor(source, canvasSizes, border) {
    this.source = source;
    this.canvasSize = canvasSizes;
    this.border = border;
  }
}

export const yandex = new Sources({
  source:'yandex',
  canvasSize:[[240, 400], [728, 90], [300, 250], [300, 600], [336, 280], [300, 500], [970, 250], [640, 100], [640, 200], [640, 960], [960, 640]],
  border:'1px',
});